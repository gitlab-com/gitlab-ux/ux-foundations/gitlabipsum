# GitLab-ipsum

A GitLab-flavored text generator. Instead of dumping Lorem Ipsum or randomly spouting words, this pulls from actual GitLab text and generates paragraphs based on your options. Format the output with the available settings, and regenerate on the fly until you have the example text you need.


## Contributing

Merge requests welcome! Please feel free to add more dictionaries, functionality, or otherwise!


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

