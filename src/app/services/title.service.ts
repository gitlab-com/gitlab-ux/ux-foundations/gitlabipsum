import { Injectable } from '@angular/core';
import { VINEYARDS } from './vineyards.dict';

@Injectable()
export class TitleService {

    public sourceTitles: string[];

    constructor() {}

    resetSource() {
        this.sourceTitles = VINEYARDS.slice(0);
    }

    genTitle() {
        if (this.sourceTitles.length === 0) {
            this.resetSource();
        }

        const i = Math.floor(Math.random() * this.sourceTitles.length);
        const selected = this.sourceTitles[i];
        this.sourceTitles.splice(i, 1);
        return selected;
    }

}
