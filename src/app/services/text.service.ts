import { Injectable } from '@angular/core';
import { ISSUABLES } from './issuables.dict';
import { DOCS } from './docs.dict';
import { HANDBOOK } from './handbook.dict';

@Injectable()
export class TextService {

    public sourceSentences: string[];
    public output: string[];
    public sourceSelection = 'wine';

    constructor() {
        this.resetSource();
    }

    resetSource() {

        // copy fresh set from database
        if (this.sourceSelection === 'issuables') {
            this.sourceSentences = ISSUABLES.slice(0);

        } else if (this.sourceSelection === 'docs') {
            this.sourceSentences = DOCS.slice(0);
            
        } else if (this.sourceSelection === 'handbook') {
            this.sourceSentences = HANDBOOK.slice(0);
            
        } else {
            this.sourceSentences = ISSUABLES.slice(0);
        }
    }

    genSentence() {

        // ran out of sentencecs? reset source
        if (this.sourceSentences.length === 0) {
            this.resetSource();
        }

        // return a setnence and remove it from source
        const i = Math.floor(Math.random() * this.sourceSentences.length);
        const selected = this.sourceSentences[i];
        this.sourceSentences.splice(i, 1);
        return selected;

    }

    genParagraphs( numParas: number, minSens: number, maxSens: number, source) {

        this.sourceSelection = source;
        this.resetSource();

        this.output = [];

        let currentParas = 0;

        while ( currentParas < numParas ) {

            // pick a random number of sentences
            const numSentences = Math.floor(Math.random() * maxSens) + minSens;
            let currentSentences = 0;
            let newPara = '';

            // add a few sentences
            while ( numSentences > currentSentences ) {
                newPara = newPara + this.genSentence() + ((numSentences === currentSentences + 1 ) ? '' : ' ');
                currentSentences = currentSentences + 1;
            }

            this.output.push(newPara);

            currentParas = currentParas + 1;

        }

        return this.output;
    }

}



