import { Component, OnInit, OnChanges, Pipe, PipeTransform } from '@angular/core';
import { TextService } from '../services/text.service';

@Component({
  selector: 'app-text-generator',
  templateUrl: './text-generator.component.html',
  styleUrls: ['./text-generator.component.scss']
})
export class TextGeneratorComponent implements OnInit {

  public outText: string[];
  public targetParagraphs = 6;
  public minSentences = 1;
  public maxSentences = 5;
  public formatMacro = 'text';
  public formatMicro = 'p';
  public sourceSelection = 'handbook';

  constructor(
    private textService: TextService
  ) {

    if (localStorage.getItem('targetParagraphs') !== null) {
      this.targetParagraphs = parseInt(localStorage.getItem('targetParagraphs'), 10);
    }

    if (localStorage.getItem('minSentences') !== null) {
      this.minSentences = parseInt(localStorage.getItem('minSentences'), 10);
    }

    if (localStorage.getItem('maxSentences') !== null) {
      this.maxSentences = parseInt(localStorage.getItem('maxSentences'), 10);
    }

    if (localStorage.getItem('formatMacro') !== null) {
      this.formatMacro = localStorage.getItem('formatMacro');
    }

    if (localStorage.getItem('formatMicro') !== null) {
      this.formatMicro = localStorage.getItem('formatMicro');
    }

    if (localStorage.getItem('sourceSelection') !== null) {
      this.sourceSelection = localStorage.getItem('sourceSelection');
    }
  }

  ngOnInit() {
    this.generate();
  }

  generate() {
    this.outText = this.textService.genParagraphs(this.targetParagraphs, this.minSentences, this.maxSentences, this.sourceSelection);
  }



  // Paragraph Options

  doParaInc() {
    this.targetParagraphs = this.targetParagraphs + 1;
    localStorage.setItem('targetParagraphs', this.targetParagraphs.toString());
    this.generate();
  }

  doParaDec() {
    if (this.targetParagraphs > 1) { this.targetParagraphs = this.targetParagraphs - 1; }
    localStorage.setItem('targetParagraphs', this.targetParagraphs.toString());
    this.generate();
  }



  // sentence number options

  doMinSenInc() {
    this.minSentences = this.minSentences + 1;
    localStorage.setItem('minSentences', this.minSentences.toString());
    if (this.minSentences > this.maxSentences) {
      this.maxSentences = this.minSentences;
      localStorage.setItem('maxSentences', this.maxSentences.toString());
    }
    this.generate();
  }

  doMinSenDec() {
    if (this.minSentences > 1) {
      this.minSentences = this.minSentences - 1;
      localStorage.setItem('minSentences', this.minSentences.toString());
    }

    this.generate();
  }

  doMaxSenInc() {
    this.maxSentences = this.maxSentences + 1;
    localStorage.setItem('maxSentences', this.maxSentences.toString());
    this.generate();
  }

  doMaxSenDec() {
    if (this.maxSentences > 1) {
      this.maxSentences = this.maxSentences - 1;
      localStorage.setItem('maxSentences', this.maxSentences.toString());
    }
    if (this.minSentences > this.maxSentences) {
      this.minSentences = this.maxSentences;
      localStorage.setItem('minSentences', this.minSentences.toString());
    }
    this.generate();
  }



  // format options

  doFormatMacro(format: string) {
    this.formatMacro = format;
    localStorage.setItem('formatMacro', this.formatMacro);
  }

  doFormatMicro(format: string) {
    this.formatMicro = format;
    localStorage.setItem('formatMicro', this.formatMicro);
  }



  // source options

  doSelectSource(sourceName: string) {
    this.sourceSelection = sourceName;
    localStorage.setItem('sourceSelection', this.sourceSelection);
    this.generate();
  }




  // reset to defaults

  doStorageReset() {
    this.targetParagraphs = 6;
    this.minSentences = 2;
    this.maxSentences = 4;
    this.formatMacro = 'text';
    this.formatMicro = 'p';
    this.sourceSelection = 'wine';
    localStorage.setItem('targetParagraphs', '6');
    localStorage.setItem('minSentences', '2');
    localStorage.setItem('maxSentences', '4');
    localStorage.setItem('formatMacro', 'text');
    localStorage.setItem('formatMicro', 'p');
    localStorage.setItem('sourceSelection', 'wine');
    this.generate();
  }



  copyToClipboard(item) {
    document.addEventListener('copy', (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', (item));
      e.preventDefault();
      document.removeEventListener('copy', null);
    });
    document.execCommand('copy');
  }



}
