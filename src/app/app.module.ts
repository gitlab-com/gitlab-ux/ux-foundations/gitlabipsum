import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TextGeneratorComponent } from './text-generator/text-generator.component';
import { TextService } from './services/text.service';

@NgModule({
  declarations: [
    AppComponent,
    TextGeneratorComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    TextService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
